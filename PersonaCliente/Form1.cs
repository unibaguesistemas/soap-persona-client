﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PersonaCliente
{
    public partial class Personas : Form
    {
        public Personas()
        {
            InitializeComponent();
        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            InsertarPersona ip = new InsertarPersona();
            ip.Visible = true;
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            BuscarPersona bp = new BuscarPersona();
            bp.Visible = true;
        }

        private void btnListar_Click(object sender, EventArgs e)
        {
            ListarPersonas lp = new ListarPersonas();
            lp.Visible = true;
        }
    }
}
