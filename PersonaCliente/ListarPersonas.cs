﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PersonaCliente
{
    public partial class ListarPersonas : Form
    {
        ServicioPersonaSOA.SWPersona2Client pc = null;

        public ListarPersonas()
        {
            InitializeComponent();
            pc = new ServicioPersonaSOA.SWPersona2Client();
            listar();
        }

        private void btnListar_Click(object sender, EventArgs e)
        {
            listar();
        }

        private void listar()
        {
			try
			{
				List<ServicioPersonaSOA.persona> personas = pc.listarPersonas().ToList();
				dataPersonas.Rows.Clear();
				for (int i = 0; i < personas.Count; i++)
				{
					int id = personas[i].id;
					String nombre = personas[i].nombre;
					String profesion = personas[i].profesion;
					DateTime fn = personas[i].fechaNacimiento;
					object[] row = new object[] { id, nombre, fn, profesion };
					dataPersonas.Rows.Add(row);
				}
			}
			catch (Exception)
			{
				dataPersonas.Rows.Clear();
			}            
        }
        
    }
}
