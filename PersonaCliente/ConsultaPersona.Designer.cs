﻿namespace PersonaCliente
{
	partial class ConsultaPersona
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnEliminar = new System.Windows.Forms.Button();
			this.btnActualizar = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.lbId = new System.Windows.Forms.Label();
			this.lbNombre = new System.Windows.Forms.Label();
			this.lbFn = new System.Windows.Forms.Label();
			this.lbProfesion = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// btnEliminar
			// 
			this.btnEliminar.Location = new System.Drawing.Point(259, 255);
			this.btnEliminar.Name = "btnEliminar";
			this.btnEliminar.Size = new System.Drawing.Size(75, 23);
			this.btnEliminar.TabIndex = 1;
			this.btnEliminar.Text = "Eliminar";
			this.btnEliminar.UseVisualStyleBackColor = true;
			this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
			// 
			// btnActualizar
			// 
			this.btnActualizar.Location = new System.Drawing.Point(57, 255);
			this.btnActualizar.Name = "btnActualizar";
			this.btnActualizar.Size = new System.Drawing.Size(75, 23);
			this.btnActualizar.TabIndex = 2;
			this.btnActualizar.Text = "Actualizar";
			this.btnActualizar.UseVisualStyleBackColor = true;
			this.btnActualizar.Click += new System.EventHandler(this.btnActualizar_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(54, 42);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(70, 13);
			this.label1.TabIndex = 3;
			this.label1.Text = "Identificación";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(54, 92);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(44, 13);
			this.label2.TabIndex = 4;
			this.label2.Text = "Nombre";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(54, 142);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(108, 13);
			this.label3.TabIndex = 5;
			this.label3.Text = "Fecha de Nacimiento";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(54, 195);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(51, 13);
			this.label4.TabIndex = 6;
			this.label4.Text = "Profesión";
			// 
			// lbId
			// 
			this.lbId.AutoSize = true;
			this.lbId.Location = new System.Drawing.Point(190, 42);
			this.lbId.Name = "lbId";
			this.lbId.Size = new System.Drawing.Size(41, 13);
			this.lbId.TabIndex = 7;
			this.lbId.Text = "Num Id";
			// 
			// lbNombre
			// 
			this.lbNombre.AutoSize = true;
			this.lbNombre.Location = new System.Drawing.Point(190, 92);
			this.lbNombre.Name = "lbNombre";
			this.lbNombre.Size = new System.Drawing.Size(60, 13);
			this.lbNombre.TabIndex = 8;
			this.lbNombre.Text = "Str Nombre";
			// 
			// lbFn
			// 
			this.lbFn.AutoSize = true;
			this.lbFn.Location = new System.Drawing.Point(190, 142);
			this.lbFn.Name = "lbFn";
			this.lbFn.Size = new System.Drawing.Size(43, 13);
			this.lbFn.TabIndex = 9;
			this.lbFn.Text = "date Fn";
			// 
			// lbProfesion
			// 
			this.lbProfesion.AutoSize = true;
			this.lbProfesion.Location = new System.Drawing.Point(190, 195);
			this.lbProfesion.Name = "lbProfesion";
			this.lbProfesion.Size = new System.Drawing.Size(57, 13);
			this.lbProfesion.TabIndex = 10;
			this.lbProfesion.Text = "Str Carrera";
			// 
			// ConsultaPersona
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(384, 296);
			this.Controls.Add(this.lbProfesion);
			this.Controls.Add(this.lbFn);
			this.Controls.Add(this.lbNombre);
			this.Controls.Add(this.lbId);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.btnActualizar);
			this.Controls.Add(this.btnEliminar);
			this.Name = "ConsultaPersona";
			this.Text = "ConsultaPersona";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.Button btnEliminar;
		private System.Windows.Forms.Button btnActualizar;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label lbId;
		private System.Windows.Forms.Label lbNombre;
		private System.Windows.Forms.Label lbFn;
		private System.Windows.Forms.Label lbProfesion;
	}
}