﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PersonaCliente.ServicioPersonaSOA;

namespace PersonaCliente
{
	public partial class ConsultaPersona : Form
	{
		ServicioPersonaSOA.persona p;
		ServicioPersonaSOA.SWPersona2Client pc = null;

		public ConsultaPersona(ServicioPersonaSOA.persona p)
		{
			InitializeComponent();
			pc = new ServicioPersonaSOA.SWPersona2Client();
			this.p = p;
			actualizar();
		}

		private void btnActualizar_Click(object sender, EventArgs e)
		{
			if (p != null)
			{
				ActualizarPersona ap = new ActualizarPersona(p);
				ap.Visible = true;
			}
			else
			{
				MessageBox.Show("No existe una persona con ese id");
			}
		}

		private void btnEliminar_Click(object sender, EventArgs e)
		{
			try
			{
				if (p != null)
				{
					pc.eliminarPersona(p.id);
					MessageBox.Show("Se elimino satisfactoriamente la persona " +
						p.nombre + " con el id " + p.id);
					this.Dispose();
				}
				else
				{
					MessageBox.Show("No existe la persona a eliminar");
				}
			}
			catch (Exception)
			{
				MessageBox.Show("Id Invalido");
			}
		}

		public void actualizar()
		{
			lbId.Text = p.id + "";
			lbNombre.Text = p.nombre;
			lbFn.Text = p.fechaNacimiento.ToString();
			lbProfesion.Text = p.profesion;
		}
	}
}
