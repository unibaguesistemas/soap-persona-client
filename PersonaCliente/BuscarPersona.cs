﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PersonaCliente
{
    public partial class BuscarPersona : Form
    {
        ServicioPersonaSOA.SWPersona2Client pc = null;
        public BuscarPersona()
        {
            InitializeComponent();
            pc = new ServicioPersonaSOA.SWPersona2Client();
        }

        private void btnActulizar_Click(object sender, EventArgs e)
        {
			try
			{
				int id = Int32.Parse(txtId.Text);
				ServicioPersonaSOA.persona p = pc.buscarPersona(id);
				if(p!= null)
				{
					ConsultaPersona cp = new ConsultaPersona(p);
					cp.Visible = true;
				}
				else
				{
					MessageBox.Show("No existe una persona con ese id");
				}
			}
			catch (Exception)
			{
				MessageBox.Show("ID Inocrrecto porfavor utilice un valor numerico");
			}
        }
    }
}
