﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PersonaCliente
{
    public partial class ActualizarPersona : Form
    {
        private int pid;
        private string pnombre;
        private string pprofesion;
        private DateTime pfn;

        ServicioPersonaSOA.SWPersona2Client pc = null;
        public ActualizarPersona(ServicioPersonaSOA.persona p)
        {
            InitializeComponent();
            pc = new ServicioPersonaSOA.SWPersona2Client();
            pid = p.id;
            pnombre = p.nombre;
            pprofesion = p.profesion;
            pfn = p.fechaNacimiento;
            actualizar();
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            try
            {
                int id = Int32.Parse(txtId.Text);
                string nombre = txtNombre.Text;
                string profesion = txtProfesion.Text;
                DateTime time = datefn.Value;
                ServicioPersonaSOA.persona pers = new ServicioPersonaSOA.persona();
                pers.id = id;
                pers.nombre = nombre;
                pers.profesion = profesion;
                pers.fechaNacimiento = time;
                pc.ModificarPersona(pid, pers);
                MessageBox.Show("Datos Actualizados :)");
                pid = id;
            }catch(Exception)
            {
                MessageBox.Show("No se pudo actualizar datos :(");
            }
        }

        public void actualizar()
        {
            txtId.Text = pid.ToString();
            txtNombre.Text = pnombre;
            txtProfesion.Text = pprofesion;
            datefn.Value = pfn;
        }
    }
}
