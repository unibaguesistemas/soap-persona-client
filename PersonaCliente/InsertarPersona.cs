﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PersonaCliente
{
    public partial class InsertarPersona : Form
    {
        ServicioPersonaSOA.SWPersona2Client pc = null;
        public InsertarPersona()
        {
            InitializeComponent();
            pc = new ServicioPersonaSOA.SWPersona2Client();
        }
        
        private void btnInsertar_Click(object sender, EventArgs e)
        {
            try
            {
                int id = Int32.Parse(txtId.Text);
                string nombre = txtNombre.Text;
                string profesion = txtProfesion.Text;
                DateTime fecha = datefn.Value;
                ServicioPersonaSOA.persona p = new ServicioPersonaSOA.persona();
                p.id = id;
                p.nombre = nombre;
                p.profesion = profesion;
                p.fechaNacimiento = fecha;
                bool request = pc.agregarPersona(p);
                if (request)
                {
                    MessageBox.Show("Persona creada");
                }
                else
                {
                    MessageBox.Show("Persona no creada, Error en algunos datos");
                }
            }
            catch
            {
                MessageBox.Show("Datos invalidos");              
            }
        }
    }


}
